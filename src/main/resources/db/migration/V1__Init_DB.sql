CREATE TABLE Users (
    id int primary key auto_increment,
    firstname varchar(100) not null,
    lastname varchar(100) not null,
    birthDate date not null,
    email varchar(100) unique not null,
    password varchar(100) not null,
    img text not null
    );

CREATE TABLE Role(
    id int primary key auto_increment,
    name varchar(100) not null
    );

CREATE TABLE Users_Roles(
    User_id int references Users (id),
    Role_id int references Role (id),
    primary key(User_id, Role_id)
    );

CREATE TABLE imageVersion(
    id int primary key auto_increment,
    img_id int not null,
    User_id int unique references Users (id)
    );

INSERT INTO Users (firstname, lastname, birthDate, email, password, img) VALUES
    ("Max", "Bat", "2022-03-08", "maxbat@bk.ru", "$2y$10$QqMgb7j5XBneog0MT2g1tueM5wAzkyhEH5BgooOZnM5I/dV/eLJm6", "http://localhost:8080/img/1_1"),
    ("test", "test", "2022-03-13", "test@test.com", "$2y$10$cftqyGm/fmogvGgUH.RgQeZE2s18ATZB1gKx2eZn5ldTWoh7VDMUu", "http://localhost:8080/img/2_1");

INSERT INTO Role (name) VALUES
    ("ROLE_ADMIN");

INSERT into users_roles (User_id, Role_id) VALUES
    (1, 1);

INSERT INTO imageVersion (User_id, img_id) VALUES
    (1, 2),
    (2, 2);
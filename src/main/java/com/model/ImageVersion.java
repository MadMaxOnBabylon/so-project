package com.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name="imageVersion")
@Data
@Accessors(chain = true)
public class ImageVersion {

    @Id
    @GeneratedValue
    private int id;
    private int imgId;
    private User userId;




}

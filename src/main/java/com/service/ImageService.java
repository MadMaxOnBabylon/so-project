package com.service;

import com.model.User;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {
    byte[] getUserImg(String img);
    void addPicToFile(MultipartFile img, User user);
    int getImgId(Long userId);
    void updateAndAddImgId(Long userId, int newId);
}

package com.service;

import com.dao.ImageVersionDao;
import com.dao.UserDao;
import com.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService{

    private final UserDao userDao;
    private final ImageVersionDao imageVersionDao;
    private final static String path = "src\\main\\resources\\userPhotos\\%sUserPhoto\\%s_%s.jpg";

    @Override
    public byte[] getUserImg(String img) {
        String[] ids = img.split("_", 2);
        try {
            byte[] photo = Files.readAllBytes(Paths.get(String.format(path, ids[0], ids[0], ids[1])));
            return photo;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addPicToFile(MultipartFile img, User user){
        int currentImgId = getImgId(user.getId());
        try {
            Files.write(Paths.get(String.format(path, user.getId(), user.getId(), currentImgId + 1)), img.getBytes());
            updateAndAddImgId(user.getId(), currentImgId+1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public int getImgId(Long userId) {
        return imageVersionDao.getImgId(userId);
    }

    @Override
    @Transactional
    public void updateAndAddImgId(Long userId, int newId) {
        imageVersionDao.updateAndAddImgId(userId, newId);
    }


}

package com.service;

import com.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;


public interface UserService extends UserDetailsService {

    void saveUser(User user);

    void removeUserById(long id);
}

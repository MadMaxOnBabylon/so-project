package com.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    @ApiOperation("возвращает страницу регистрации")
    @GetMapping
    public String registration() {
        return "registration";
    }
}

package com.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    @ApiOperation("возвращает страницу аутентификации")
    @GetMapping
    public String login() {
        return "login";
    }
}

package com.converter;


import com.dto.UserDtoProfile;
import com.model.User;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class  UserConverter {

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    public UserDtoProfile userDtoProfile (User user){
        return new UserDtoProfile()
                .setFirstName(user.getFirstName())
                .setLastName(user.getLastName())
                .setBirthDate(formatter.format(user.getBirthDate()))
                .setEmail(user.getEmail())
                .setProfileImg(user.getProfileImg());

    }
}

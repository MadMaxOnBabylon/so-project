package com.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@ApiModel(description = "Информация доступная в профайле")
public class UserDtoProfile {

    @ApiModelProperty(notes = "Имя")
    private String firstName;

    @ApiModelProperty(notes = "Фамилия")
    private String lastName;

    @ApiModelProperty(notes = "Дата рождения")
    private String birthDate;

    @ApiModelProperty(notes = "Почта")
    private String email;

    @ApiModelProperty(notes = "Фото")
    private String profileImg;
}

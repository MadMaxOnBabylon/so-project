package com.restController;

import com.converter.UserConverter;
import com.dto.UserDtoProfile;
import com.model.User;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserRestController {

    private final UserConverter userConverter;

    @GetMapping("/api/user/profile")
    @ApiOperation("Выдает данные о пользователе")
    public UserDtoProfile getProfile(@AuthenticationPrincipal User user){
        return userConverter.userDtoProfile(user);
    }
}

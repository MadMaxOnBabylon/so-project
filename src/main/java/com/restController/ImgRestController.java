package com.restController;

import com.model.ImageVersion;
import com.model.User;
import com.service.ImageServiceImpl;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
public class ImgRestController {

    private final ImageServiceImpl imageService;

    @GetMapping("/img/{imgId}")
    @ApiOperation("Загружает аватар пользователя")
    public byte[] getImgUser(@PathVariable String imgId){
        return imageService.getUserImg(imgId);
    }

    @PostMapping("/img/save")
    public ResponseEntity saveUserImg(@RequestBody MultipartFile img, @AuthenticationPrincipal User user){
        imageService.addPicToFile(img, user);
        return new ResponseEntity(HttpStatus.OK);
    }
}

package com.dao;

public interface ImageVersionDao {

    Integer getImgId(Long userId);
    void updateAndAddImgId(Long userId, int newId);
}

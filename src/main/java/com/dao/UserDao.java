package com.dao;

import com.model.User;

public interface UserDao {

    void saveUser(User user);

    void removeUserById(long id);

    User getByLogin(String email);

}

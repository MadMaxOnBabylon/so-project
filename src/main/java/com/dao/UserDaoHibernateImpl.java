package com.dao;

import com.model.User;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


@Component
public class UserDaoHibernateImpl implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveUser(User user) {
        entityManager.merge(user);
    }

    @Override
    public void removeUserById(long id) {
        entityManager.createQuery("Delete from User u Where u.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public User getByLogin(String email) {
        TypedQuery<User> typedQuery = entityManager.createQuery("Select u from User u Where u.email = :email", User.class)
                .setParameter("email", email);
        return typedQuery.getSingleResult();
    }


}

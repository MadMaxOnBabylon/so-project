package com.dao;


import com.model.ImageVersion;
import org.springframework.stereotype.Component;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class ImageVersionDaoImpl implements ImageVersionDao{

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public Integer getImgId(Long userId) {
        return entityManager.createQuery("Select img from ImageVersion Where img.id = :id", Integer.class)
                .setParameter("id", userId)
                .getResultStream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void updateAndAddImgId(Long userId, int newId) {
        if(getImgId(userId) != null) {
            entityManager.createQuery("Update ImageVersion img set img.id = :newId")
                    .setParameter("newId", newId)
                    .executeUpdate();
        }
        else {
            entityManager.createQuery("Insert into ImageVersion img (img.imgId, img.userId ) value (:newId, :userId)")
                    .setParameter("newId", newId)
                    .setParameter("userId", userId)
                    .executeUpdate();
        }
    }
}

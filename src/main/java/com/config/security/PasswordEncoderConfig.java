package com.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordEncoderConfig {

    private final int strength = 10;

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder(strength);
    }
}
